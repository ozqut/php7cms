<?php namespace Config;

use CodeIgniter\Config\Services as CoreServices;

require_once BASEPATH.'Config/Services.php';

/**
 * Services Configuration file.
 *
 * Services are simply other classes/libraries that the system uses
 * to do its job. This is used by CodeIgniter to allow the core of the
 * framework to be swapped out easily without affecting the usage within
 * the rest of your application.
 *
 * This file holds any application-specific services, or service overrides
 * that you might need. An example has been included with the general
 * method format you should use for your service methods. For more examples,
 * see the core Services file at system/Config/Services.php.
 */
class Services extends CoreServices
{

//    public static function example($getShared = true)
//    {
//        if ($getShared)
//        {
//            return self::getSharedInstance('example');
//        }
//
//        return new \CodeIgniter\Example();
//    }


    public static function exceptions(\Config\App $config = null, $getShared = true)
    {
        if ($getShared)
        {
            return self::getSharedInstance('exceptions', $config);
        }

        if (empty($config))
        {
            $config = new \Config\App();
        }

        return new \Phpcmf\Extend\Exceptions($config);
    }



    public static function request(\Config\App $config = null, $getShared = true)
    {

        if ($getShared)
        {
            return self::getSharedInstance('request', $config);
        }

        if (! is_object($config))
        {
            $config = new \Config\App();
        }


        return new \Phpcmf\Extend\Request(
            $config,
            new \CodeIgniter\HTTP\URI()
        );
    }

    public static function renderer($viewPath = APPPATH.'Views/', $config = null, $getShared = true)
    {
        if ($getShared)
        {
            return self::getSharedInstance('renderer', $viewPath, $config);
        }

        if (is_null($config))
        {
            $config = new \Config\View();
        }

        return new \Phpcmf\Extend\View($config, $viewPath, self::locator(true), CI_DEBUG, self::logger(true));
    }


    public static function security(\Config\App $config = null, $getShared = true)
    {
        if ($getShared)
        {
            return self::getSharedInstance('security', $config);
        }

        if (! is_object($config))
        {
            $config = new \Config\App();
        }

        return new \Phpcmf\Extend\Security($config);
    }

}

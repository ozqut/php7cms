<?php namespace Phpcmf\Controllers\Api;

// 接口处理
class Home extends \Phpcmf\Common
{

	public function index() {
       $myfile = COREPATH.'Api/'.ucfirst(IS_API).'.php';

        !is_file($myfile) && exit('api file is error');

        require $myfile;
        exit;
	}

}

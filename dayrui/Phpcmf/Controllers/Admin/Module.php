<?php namespace Phpcmf\Controllers\Admin;

class Module extends \Phpcmf\Common
{

    private $dir;
    private $form;

    public function __construct(...$params) {
        parent::__construct(...$params);

        $this->dir = dr_safe_replace(\Phpcmf\Service::L('Input')->get('dir'));

        $menu = [
            '内容模块' => [\Phpcmf\Service::L('Router')->class.'/index', 'fa fa-cogs'],
            '模块配置' => ['hide:'.\Phpcmf\Service::L('Router')->class.'/edit', 'fa fa-cog'],
            '推荐位配置' => ['hide:'.\Phpcmf\Service::L('Router')->class.'/flag_edit', 'fa fa-flag'],
            'help' => [57]
        ];



        \Phpcmf\Service::V()->assign('menu', \Phpcmf\Service::M('auth')->_admin_menu($menu));

        // 表单验证配置
        $this->form = [
            'name' => [
                'name' => '表单名称',
                'rule' => [
                    'empty' => dr_lang('表单名称不能为空')
                ],
                'filter' => [],
                'length' => '200'
            ],
            'table' => [
                'name' => '数据表名称',
                'rule' => [
                    'empty' => dr_lang('数据表名称不能为空'),
                    'table' => dr_lang('数据表名称格式不正确'),
                ],
                'filter' => [],
                'length' => '200'
            ],
        ];
    }



    // 安装模块
    public function install() {

        $dir = dr_safe_replace(\Phpcmf\Service::L('Input')->get('dir'));
        $type = (int)\Phpcmf\Service::L('Input')->get('type');

        !preg_match('/^[a-z]+$/U', $dir) && $this->_json(0, dr_lang('模块目录[%s]格式不正确', $dir));

        $path = APPSPATH.ucfirst($dir).'/';
        !is_dir($path) && $this->_json(0, dr_lang('模块目录[%s]不存在', $path));

        // 对当前模块属性判断
        $cfg = require $path.'Config/App.php';
        !$cfg && $this->_json(0, dr_lang('文件[%s]不存在', 'App/'.ucfirst($dir).'/Config/App.php'));

        $cfg['share'] = $type ? 0 : 1;

        $rt = \Phpcmf\Service::M('Module')->install($dir, $cfg);
        $this->_json($rt['code'], $rt['msg']);
    }

    // 卸载模块
    public function uninstall() {

        $dir = dr_safe_replace(\Phpcmf\Service::L('Input')->get('dir'));
        !preg_match('/^[a-z]+$/U', $dir) && $this->_json(0, dr_lang('模块目录[%s]格式不正确', $dir));

        $path = APPSPATH.ucfirst($dir).'/';
        !is_dir($path) && $this->_json(0, dr_lang('模块目录[%s]不存在', $path));

        $cfg = require $path.'Config/App.php';
        !$cfg && $this->_json(0, dr_lang('文件[%s]不存在', 'App/'.ucfirst($dir).'/Config/App.php'));

        $rt = \Phpcmf\Service::M('Module')->uninstall($dir, $cfg);
        $this->_json($rt['code'], $rt['msg']);

    }

    // 模块管理
    public function index() {

        $list = [];
        $local = dr_dir_map(APPSPATH, 1); // 搜索本地模块
        foreach ($local as $dir) {
            if (is_file(APPSPATH.$dir.'/Config/App.php')) {
                $key = strtolower($dir);
                $cfg = require APPSPATH.$dir.'/Config/App.php';
                if ($cfg['type'] == 'module') {
                    $cfg['dirname'] = $key;
                    $list[$key] = $cfg;
                }
            }
        }

        $my = [];
        $module = \Phpcmf\Service::M('Module')->All(); // 库中已安装模块
        if ($module) {
            foreach ($module as $t) {
                $dir = $t['dirname'];
                if ($list[$dir]) {
                    $t['key'] = $t['key'];
                    $t['name'] = $list[$dir]['name'];
                    $t['mtype'] = $list[$dir]['mtype'];
                    $t['system'] = $list[$dir]['system'];
                    $t['version'] = $list[$dir]['version'];
                    $site = dr_string2array($t['site']);
                    $t['install'] = isset($site[SITE_ID]) && $site[SITE_ID] ? 1 : 0;
                    $t['site'] = count($site);
                    $my[$dir] = $t;
                    unset($list[$dir]);
                }
            }
        }

        \Phpcmf\Service::V()->assign([
            'my' => $my,
            'list' => $list,
        ]);
        \Phpcmf\Service::V()->display('module_list.html');
    }

    public function displayorder_edit() {

        // 查询数据
        $id = (int)\Phpcmf\Service::L('Input')->get('id');
        $row = \Phpcmf\Service::M('Module')->table('module')->get($id);
        !$row && $this->_json(0, dr_lang('数据#%s不存在', $id));

        $value = (int)\Phpcmf\Service::L('Input')->get('value');
        $rt = \Phpcmf\Service::M('Module')->table('module')->save($id, 'displayorder', $value);
        !$rt['code'] && $this->_json(0, $rt['msg']);

        \Phpcmf\Service::L('Input')->system_log('修改模块('.$row['dirname'].')的排序值为'.$value);
        $this->_json(1, dr_lang('操作成功'));
    }

    // 隐藏或者启用
    public function hidden_edit() {

        $id = (int)\Phpcmf\Service::L('Input')->get('id');
        $row = \Phpcmf\Service::M('Module')->table('module')->get($id);
        !$row && $this->_json(0, dr_lang('数据#%s不存在', $id));

        $v = $row['disabled'] ? 0 : 1;
        \Phpcmf\Service::M('Module')->table('module')->update($id, ['disabled' => $v]);

        exit($this->_json(1, dr_lang($v ? '模块已被禁用' : '模块已被启用'), ['value' => $v]));
    }

    public function edit() {

        $id = (int)\Phpcmf\Service::L('Input')->get('id');
        $data = \Phpcmf\Service::M()->table('module')->get($id);
        !$data && $this->_admin_msg(0, dr_lang('数据#%s不存在', $id));

        // 格式转换
        $data['site'] = dr_string2array($data['site']);
        $data['setting'] = dr_string2array($data['setting']);

        // 判断站点
        !$data['site'][SITE_ID] && $this->_admin_msg(0, dr_lang('当前站点尚未安装'));

        // 默认显示字段
        !$data['setting']['list_field'] && $data['setting']['list_field'] = [
            'title' => [
                'use' => 1,
                'name' => dr_lang('主题'),
                'func' => 'title',
                'width' => 0,
                'order' => 1,
            ],
            'catid' => [
                'use' => 1,
                'name' => dr_lang('栏目'),
                'func' => 'catid',
                'width' => 120,
                'order' => 2,
            ],
            'author' => [
                'use' => 1,
                'name' => dr_lang('作者'),
                'func' => 'author',
                'width' => 100,
                'order' => 3,
            ],
            'updatetime' => [
                'use' => 1,
                'name' => dr_lang('更新时间'),
                'func' => 'datetime',
                'width' => 160,
                'order' => 4,
            ],
        ];
        // 默认显示字段
        !$data['setting']['comment_list_field'] && $data['setting']['comment_list_field'] = [
            'content' => [
                'use' => 1,
                'name' => dr_lang('评论'),
                'func' => 'comment',
                'width' => 0,
                'order' => 1,
            ],
            'author' => [
                'use' => 1,
                'name' => dr_lang('作者'),
                'func' => 'author',
                'width' => 100,
                'order' => 3,
            ],
            'inputtime' => [
                'use' => 1,
                'name' => dr_lang('评论时间'),
                'func' => 'datetime',
                'width' => 150,
                'order' => 4,
            ],
        ];


        if (IS_AJAX_POST) {
            $post = \Phpcmf\Service::L('Input')->post('data', true);
            $rt = \Phpcmf\Service::M('Module')->config($data, $post);
            $rt['code'] ? $this->_json(1, '操作成功') : $this->_json(0, $rt['msg']);
        }

        // 主表字段
        $field = \Phpcmf\Service::M()->db->table('field')
            ->where('disabled', 0)
            ->where('ismain', 1)
            ->where('relatedname', 'module')
            ->where('relatedid', $id)
            ->orderBy('displayorder ASC,id ASC')
            ->get()->getResultArray();
        $sys_field = \Phpcmf\Service::L('Field')->sys_field(['catid', 'author', 'inputtime', 'updatetime']);
        sort($sys_field);
        $field = dr_array2array($sys_field, $field);

        // 评论字段
        $comment_field = \Phpcmf\Service::M()->db->table('field')
            ->where('disabled', 0)
            ->where('ismain', 1)
            ->where('relatedname', 'comment-module-'.$data['dirname'])
            ->orderBy('displayorder ASC,id ASC')
            ->get()->getResultArray();
        $sys_field = \Phpcmf\Service::L('Field')->sys_field(['content', 'author', 'inputtime']);
        $comment_field = dr_array2array($sys_field, $comment_field);


        $page = intval(\Phpcmf\Service::L('Input')->get('page'));

        if (!$data['site'][SITE_ID]['title']) {
            $cfg = require APPSPATH.ucfirst($data['dirname']).'/Config/App.php';
            $data['site'][SITE_ID]['title'] = $cfg['name'];
        }

        \Phpcmf\Service::V()->assign([
            'page' => $page,
            'data' => $data,
            'site' => $data['site'][SITE_ID],
            'form' => dr_form_hidden(['page' => $page]),
            'field' => $field,
            'comment_field' => $comment_field,
        ]);
        \Phpcmf\Service::V()->display('module_edit.html');
    }

    // 推荐位
    public function flag_edit() {

        $id = (int)\Phpcmf\Service::L('Input')->get('id');
        $data = \Phpcmf\Service::M('Module')->table('module')->get($id);
        !$data && $this->_admin_msg(0, dr_lang('数据#%s不存在', $id));

        // 格式转换
        $data['setting'] = dr_string2array($data['setting']);

        if (IS_AJAX_POST) {
            $post = \Phpcmf\Service::L('Input')->post('flag', true);
            $rt = \Phpcmf\Service::M('Module')->config($data, null, [
                'flag' => $post,
            ]);
            $rt['code'] ? $this->_json(1, '操作成功') : $this->_json(0, $rt['msg']);
        }

        \Phpcmf\Service::V()->assign([
            'flag' => $data['setting']['flag'],
            'form' => dr_form_hidden(),
        ]);
        \Phpcmf\Service::V()->display('module_flag.html');
    }




    // 验证数据
    private function _validation($id, $data) {

        list($data, $return) = \Phpcmf\Service::L('Form')->validation($data, $this->form);
        $return && exit($this->_json(0, $return['error'], ['field' => $return['name']]));
        \Phpcmf\Service::M()->table('module_form')->where('module', $this->dir)->is_exists($id, 'table', $data['table']) && exit($this->_json(0, dr_lang('数据表名称已经存在'), ['field' => 'table']));
    }




}

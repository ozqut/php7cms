<?php namespace Phpcmf\Library;

/**
 * 存储
 */
class Storage {

    public $ci;

    // 存储对象
    protected $object;

    private function _init($attachment) {


        $this->object = new \Phpcmf\ThirdParty\Storage\Local();

    }

    // 文件上传
    public function upload($type, $data, $file_path, $attachment, $watermarkk) {

        $this->_init($attachment);
        return $this->object->init($attachment, $file_path)->upload($type, $data, $watermarkk);
    }

    // 文件删除
    public function delete($attachment, $filename) {

        $this->_init($attachment);
        return $this->object->init($attachment, $filename)->delete();
    }
}
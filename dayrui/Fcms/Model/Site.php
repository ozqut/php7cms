<?php namespace Phpcmf\Model;

class Site extends \Phpcmf\Model
{

    // 获取网站配置
    public function config($siteid, $name = '', $data = []) {

        !$siteid && $siteid = SITE_ID;
        $site = $this->table('site')->get($siteid);
        if (!$site) {
            return [];
        }

        $site['setting'] = dr_string2array($site['setting']);
        !$site['setting']['config']['SITE_NAME'] && $site['setting']['config']['SITE_NAME'] = $site['name'];
        !$site['setting']['config']['SITE_DOMAIN'] && $site['setting']['config']['SITE_DOMAIN'] = $site['domain'];

        if ($name && $data) {
            // 更新数据
            $data['SITE_NAME'] && $site['name'] = $data['SITE_NAME'];
            $data['SITE_DOMAIN'] && $site['domain'] = $data['SITE_DOMAIN'];
            $site['setting'][$name] = $data;
            $this->table('site')->update($siteid, [
                'name' => $site['name'],
                'domain' => $site['domain'],
                'setting' => dr_array2string($site['setting']),
            ]);
        }

        return $site['setting'];
    }

    // 设置网站单个配置
    public function config_value($siteid, $group, $value) {

        !$siteid && $siteid = SITE_ID;
        $site = $this->table('site')->get($siteid);
        if (!$site || !$value) {
            return;
        }

        $site['setting'] = dr_string2array($site['setting']);

        foreach ($value as $n => $v) {
            $site['setting'][$group][$n] = $v;
        }

        $this->table('site')->update($siteid, [
            'setting' => dr_array2string($site['setting']),
        ]);

        return;
    }



    // 设置域名
    public function domain($value = []) {

        $data = [];
        $site = $this->config(SITE_ID);
        if ($value) {
            $site['config']['SITE_DOMAIN'] = $value['site_domain'] ? $value['site_domain'] : $site['config']['SITE_DOMAIN'];
            $site['config']['SITE_DOMAINS'] = $value['site_domains'];
            $site['mobile']['domain'] = $value['mobile_domain'];
            $this->db->table('site')->where('id', SITE_ID)->update([
                'domain' => $site['config']['SITE_DOMAIN'],
                'setting' => dr_array2string($site),
            ]);
        }

        $data['site_domain'] = $site['config']['SITE_DOMAIN'];
        $data['site_domains'] = $site['config']['SITE_DOMAINS'];
        $data['mobile_domain'] = $site['mobile']['domain'];

        // 用户中心域名
        if (dr_is_app('member')) {
            $member = $this->db->table('member_setting')->where('name', 'domain')->get()->getRowArray();
            $member && $member['value'] = dr_string2array($member['value']);
            if ($value) {
                $member['value'][SITE_ID]['domain'] = $value['member_domain'];
                $member['value'][SITE_ID]['mobile_domain'] = $value['member_mobile_domain'];
                $this->db->table('member_setting')->replace([
                    'name' => 'domain',
                    'value' => dr_array2string($member['value'])
                ]);
            }
            $data['member_domain'] = $member['value'][SITE_ID]['domain'];
            $data['member_mobile_domain'] = $member['value'][SITE_ID]['mobile_domain'];
        }

        // 模块域名
        $my = [];
        $module = $this->table('module')->getAll();
        foreach ($module as $t) {
            if (!is_file(APPSPATH.ucfirst($t['dirname']).'/Config/App.php')) {
                continue;
            }
            $cfg = require APPSPATH.ucfirst($t['dirname']).'/Config/App.php';
            $t['site'] = dr_string2array($t['site']);
            $my[$t['dirname']] = [
                'name' => dr_lang($cfg['name']),
                'error' => '',
            ];
            if ($t['share']) {
                $my[$t['dirname']]['error'] = dr_lang('共享模块不支持绑定');
                continue;
            }
            if ($t['site'][SITE_ID]) {
                if ($value) {
                    $t['site'][SITE_ID]['domain'] = $value['module_'.$t['dirname']];
                    $t['site'][SITE_ID]['mobile_domain'] = $value['module_mobile_'.$t['dirname']];
                    $this->db->table('module')->where('id', $t['id'])->update([
                        'site' => dr_array2string($t['site'])
                    ]);
                }
                $data['module_'.$t['dirname']] = $t['site'][SITE_ID]['domain'];
                $data['module_mobile_'.$t['dirname']] = $t['site'][SITE_ID]['mobile_domain'];
            } else {
                $my[$t['dirname']]['error'] = dr_lang('当前站点未安装');
            }
        }

        return [$my, $data];
    }


    // 站点缓存缓存
    public function cache($siteid = null, $data = null, $module = null) {

        !$data && $data = $this->table('site')->getAll();
        $client_domain = $app_domain = $site_domain = $config = $cache = [];
        if ($data) {
            foreach ($data as $t) {
                $t['setting'] = dr_string2array($t['setting']);
                if ($t['setting']['config']) {
                    foreach ($t['setting']['config'] as $i => $v) {
                        if ($i == 'SITE_DOMAINS' && $v) {
                            $v = explode(',', str_replace(',,', ',', str_replace([chr(13), PHP_EOL], ',', $v)));
                            if ($v) {
                                foreach ($v as $tt) {
                                    $tt && $site_domain[$tt] = $t['id'];
                                }
                            }
                            $t['setting']['config']['SITE_DOMAINS'] = $v;
                        }
                    }
                }
                $logo = dr_get_file($t['setting']['config']['logo']);
                $config[$t['id']] = [
                    'SITE_NAME' => $t['name'],
                    'SITE_DOMAIN' => $t['domain'],
                    'SITE_DOMAINS' => $t['setting']['config']['SITE_DOMAINS'],
                    'SITE_LOGO' => $logo ? $logo : THEME_PATH.'assets/logo-web.png',
                    'SITE_MOBILE' => (string)$t['setting']['mobile']['domain'],
                    'SITE_AUTO' => (string)$t['setting']['mobile']['auto'],
                    'SITE_CLOSE' => $t['setting']['config']['SITE_CLOSE'],
                    'SITE_THEME' => $t['setting']['config']['SITE_THEME'],
                    'SITE_TEMPLATE' => $t['setting']['config']['SITE_TEMPLATE'],
                    'SITE_REWRITE' => $t['setting']['seo']['SITE_REWRITE'],
                    'SITE_SEOJOIN' => $t['setting']['seo']['SITE_SEOJOIN'],
                    'SITE_LANGUAGE' => $t['setting']['config']['SITE_LANGUAGE'],
                    'SITE_TIMEZONE' => $t['setting']['config']['SITE_TIMEZONE'],
                    'SITE_TIME_FORMAT' => $t['setting']['config']['SITE_TIME_FORMAT'],
                    'SITE_INDEX_HTML' => (string)$t['setting']['config']['SITE_INDEX_HTML'],
                ];
                unset($t['setting']['mobile']['auto'],
                    $t['setting']['mobile']['domain'],
                    $t['setting']['seo']['SITE_REWRITE'],
                    $t['setting']['seo']['SITE_SEOJOIN'],
                    $t['setting']['config']['SITE_THEME'],
                    $t['setting']['config']['SITE_TEMPLATE'],
                    $t['setting']['config']['SITE_LANGUAGE'],
                    $t['setting']['config']['SITE_TIME_FORMAT'],
                    $t['setting']['config']['SITE_NAME'],
                    $t['setting']['config']['SITE_TIMEZONE'],
                    $t['setting']['config']['SITE_DOMAIN'],
                    $t['setting']['config']['SITE_CLOSE']
                );
                $cache[$t['id']] = $t['setting'];
                // 本站的全部域名归属
                $site_domain[$t['domain']] = $t['id'];
                if ($config[$t['id']]['SITE_MOBILE']) {
                    $site_domain[$config[$t['id']]['SITE_MOBILE']] = $t['id'];
                    $client_domain[$t['domain']] = $config[$t['id']]['SITE_MOBILE'];
                }
            }

            // 用户中心域名
            $member = $this->db->table('member_setting')->where('name', 'domain')->get()->getRowArray();
            $value = $member ? dr_string2array($member['value']) : [];
            if ($value) {
                foreach ($value as $i => $t) {
                    if ($t['domain']) {
                        $site_domain[$t['domain']] = $i;
                        $app_domain[$t['domain']] = 'member';
                    }
                    if ($t['mobile_domain']) {
                        $site_domain[$t['mobile_domain']] = $i;
                        $app_domain[$t['mobile_domain']] = 'member';
                        $client_domain[$t['domain']] = $t['mobile_domain'];
                    }
                }
            }

            // 模块域名
            !$module && $module = $this->table('module')->getAll();
            if ($module) {
                foreach ($module as $t) {
                    if (!is_file(APPSPATH.ucfirst($t['dirname']).'/Config/App.php')) {
                        continue;
                    }
                    $t['site'] = dr_string2array($t['site']);
                    if (!$t['site'] || $t['share']) {
                        continue;
                    }
                    foreach ($t['site'] as $sid => $v) {
                        if ($v['domain']) {
                            $site_domain[$v['domain']] = $sid;
                            $app_domain[$v['domain']] = $t['dirname'];
                        }
                        if ($v['mobile_domain']) {
                            $site_domain[$v['mobile_domain']] = $sid;
                            $app_domain[$v['mobile_domain']] = $t['dirname'];
                            $client_domain[$v['domain']] = $v['mobile_domain'];
                        }
                    }
                }
            }
        }

        \Phpcmf\Service::L('Cache')->set_file('site', $cache);
        \Phpcmf\Service::L('Config')->file(WRITEPATH.'config/site.php', '站点配置文件', 32)->to_require($config);
        \Phpcmf\Service::L('Config')->file(WRITEPATH.'config/domain_app.php', '项目域名配置文件', 32)->to_require_one($app_domain);
        \Phpcmf\Service::L('Config')->file(WRITEPATH.'config/domain_site.php', '站点域名配置文件', 32)->to_require_one($site_domain);
        \Phpcmf\Service::L('Config')->file(WRITEPATH.'config/domain_client.php', '客户端域名配置文件', 32)->to_require_one($client_domain);


    }
}
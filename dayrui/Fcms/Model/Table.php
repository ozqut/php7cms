<?php namespace Phpcmf\Model;

// 数据表
class Table extends \Phpcmf\Model
{

    // 表结构缓存
    public function cache($siteid = SITE_ID, $module = null) {

        $cache = [];
        $paytable = []; // 付款表名
        // 生成模块表结构
        !$module && $module = $this->table('module')->getAll();
        if ($module) {
            foreach ($module as $t) {
                // 模块主表
                $table = dr_module_table_prefix($t['dirname'], $siteid);
                $prefix = $this->dbprefix($table);
                // 判断是否存在表
                if (!$this->db->tableExists($prefix)) {
                    continue;
                }
                $main_field = $this->db->getFieldNames($prefix);
                if ($main_field) {
                    // 付款表
                    $paytable['module-'.$t['id']] = [
                        'table' => $table,
                        'name' => 'title',
                        'thumb' => 'thumb',
                        'url' => '/index.php?s='.$t['dirname'].'&c=show&id=',
                        'username' => 'author',
                    ];
                    // 模块表
                    $cache[$prefix] = $main_field;
                    // 模块附表
                    $table = $prefix.'_data_0';
                    $this->db->tableExists($table) && $cache[$table] = $this->db->getFieldNames($table);
                    // 栏目附加主表
                    $table = $prefix.'_category_data';
                    $this->db->tableExists($table) && $cache[$table] = $this->db->getFieldNames($table);
                    // 栏目附加附表
                    $table = $prefix.'_category_data_0';
                    $this->db->tableExists($table) && $cache[$table] = $this->db->getFieldNames($table);
                    // 模块点击量表
                    $table = $prefix.'_hits';
                    $this->db->tableExists($table) && $cache[$table] = $this->db->getFieldNames($table);
                    // 模块评论表
                    $table = $prefix.'_comment';
                    $this->db->tableExists($table) && $cache[$table] = $this->db->getFieldNames($table);

                }


            }
        }

        // 网站表单
        $form = $this->table($siteid.'_form')->getAll();
        if ($form) {
            foreach ($form as $t) {
                // 主表
                $table = $siteid.'_form_'.$t['table'];
                $prefix = $this->dbprefix($table);
                $cache[$prefix] = $this->db->getFieldNames($prefix);
                // 付款表
                $paytable['form-'.$siteid.'-'.$t['id']] = [
                    'table' => $table,
                    'name' => 'title',
                    'thumb' => 'thumb',
                    'url' => '/index.php?s=form&c='.$t['table'].'&m=show&id=',
                    'username' => 'author',
                ];
            }
        }

        // 会员表
        $table = $this->dbprefix('member');
        $cache[$table] = $this->db->getFieldNames($table);

        // 会员附表
        $table = $this->dbprefix('member_data');
        $cache[$table] = $this->db->getFieldNames($table);

        // 缓存表结构
        \Phpcmf\Service::L('cache')->set_file('table-'.$siteid, $cache);

        // 缓存options值的字段类型 + pay
        $cache = [];
        $field = $this->db->table('field')->where('disabled', 0)->whereIn('fieldtype', ['Select', 'Checkbox', 'Radio', 'Pay', 'Pays'])->orderBy('id ASC')->get()->getResultArray();
        if ($field) {
            foreach ($field as $f) {
                $f['setting'] = dr_string2array($f['setting']);
                $cache[$f['id']] = $f;
            }
        }
        \Phpcmf\Service::L('cache')->set_file('table-field', $cache);

        // 缓存付款表
        \Phpcmf\Service::L('cache')->set_file('table-pay-'.$siteid, $paytable);
        /*
         * $paytable字段主键为 自定义字段rname-rid
         * */
    }

    // 获取字段结构
    public function get_field($table) {

        return $this->db->getFieldNames($this->dbprefix($table));
    }
    
    // 执行批量sql
    public function _query($sql, $replace = []) {

        $replace[0][] = '{dbprefix}';
        $replace[1][] = $this->db->DBPrefix;

        $todo = [];
        $count = 0;
        $sql_data = explode(';SQL_FINECMS_EOL', trim(str_replace(array(PHP_EOL, chr(13), chr(10)), 'SQL_FINECMS_EOL', str_replace($replace[0], $replace[1], $sql))));
        if ($sql_data) {
            foreach($sql_data as $query){
                if (!$query) {
                    continue;
                }
                $ret = '';
                $queries = explode('SQL_FINECMS_EOL', trim($query));
                foreach($queries as $query) {
                    $ret.= $query[0] == '#' || $query[0].$query[1] == '--' ? '' : $query;
                }
                if (!$ret) {
                    continue;
                }
                if ($this->db->simpleQuery($ret)) {
                    $todo[] = $ret;
                    $count++;
                } else {
                    $rt = $this->db->error();
                    return dr_return_data(0, $rt['message'].'<br> '.$ret);
                }
            }
        }
        
        return dr_return_data(1, '', [$count, $todo]);
    }
    

    // 网站表单--------------------------------------------------------------------

    // 创建
    public function create_form($data) {

        $pre = $this->dbprefix(SITE_ID.'_form');
        $sql = [
            "
			CREATE TABLE IF NOT EXISTS `".$pre.'_'.$data['table']."` (
			  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `uid` int(10) unsigned DEFAULT 0 COMMENT '录入者uid',
			  `author` varchar(100) DEFAULT NULL COMMENT '录入者账号',
			  `title` varchar(255) DEFAULT NULL COMMENT '主题',
			  `inputip` varchar(50) DEFAULT NULL COMMENT '录入者ip',
			  `inputtime` int(10) unsigned NOT NULL COMMENT '录入时间',
	          `status` tinyint(1) DEFAULT NULL COMMENT '状态值',
			  `displayorder` int(10) NOT NULL DEFAULT '0' COMMENT '排序值',
	          `tableid` smallint(5) unsigned NOT NULL COMMENT '附表id',
			  PRIMARY KEY `id` (`id`),
			  KEY `uid` (`uid`),
			  KEY `status` (`status`),
			  KEY `inputtime` (`inputtime`),
			  KEY `displayorder` (`displayorder`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='".$data['name']."表单表';"
            ,
            "CREATE TABLE IF NOT EXISTS `".$pre.'_'.$data['table']."_data_0` (
			  `id` int(10) unsigned NOT NULL,
			  `uid` int(10) unsigned DEFAULT 0 COMMENT '录入者uid',
			  UNIQUE KEY `id` (`id`),
			  KEY `uid` (`uid`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='".$data['name']."表单附表';"
        ];

        foreach ($sql as $s) {
            $this->db->simpleQuery(trim($s));
        }

        // 默认字段
        $this->db->table('field')->insert(array(
            'name' => '主题',
            'fieldname' => 'title',
            'fieldtype' => 'Text',
            'relatedid' => $data['id'],
            'relatedname' => 'form-'.SITE_ID,
            'isedit' => 1,
            'ismain' => 1,
            'ismember' => 1,
            'issystem' => 1,
            'issearch' => 1,
            'disabled' => 0,
            'setting' => dr_array2string(array(
                'option' => array(
                    'width' => 300, // 表单宽度
                    'fieldtype' => 'VARCHAR', // 字段类型
                    'fieldlength' => '255' // 字段长度
                ),
                'validate' => array(
                    'xss' => 1, // xss过滤
                    'required' => 1, // 表示必填
                )
            )),
            'displayorder' => 0,
        ));
    }
    
    // 系统字段
    public function sys_field_form() {
        
        return [
            1 => ['id', 'title', 'uid', 'inputip', 'inputtime', 'displayorder', 'tableid'],
            0 => ['id', 'uid'],
        ];
    }
    // 自定义字段
    public function my_field_form($form) {
        
        if (!$form) {
            return [];
        }

        
    }
    
    // 删除表单
    public function delete_form($data) {

        $id = intval($data['id']);
        $pre = $this->dbprefix(SITE_ID.'_form');
        
        // 删除字段
        $this->db->table('field')->where('relatedid', $id)->where('relatedname', 'form-'.SITE_ID)->delete();

        // 删除表
        $table = $pre.'_'.$data['table'];
        $this->db->simpleQuery('DROP TABLE IF EXISTS `'.$table.'`');

        // 删除附表
        for ($i = 0; $i < 200; $i ++) {
            if (!$this->db->query("SHOW TABLES LIKE '".$table.'_data_'.$i."'")->getRowArray()) {
                break;
            }
            $this->db->simpleQuery('DROP TABLE IF EXISTS '.$table.'_data_'.$i);
        }

        // 删除菜单
        $this->db->table('admin_menu')->where('mark', 'form-'.$data['table'])->delete();
        $this->db->table('member_menu')->where('mark', 'form-'.$data['table'])->delete();
        
        // 删除记录
        $this->db->table(SITE_ID.'_form')->delete($id);
        
    }


    // 模块表单--------------------------------------------------------------------

    // 创建
    public function create_module_form($data) {



    }
    
    // 删除模块表单
    public function delete_module_form($data) {

    }
    

    // 站点--------------------------------------------------------------------

    // 创建站点
    public function create_site($siteid) {



    }


    //--------------------------------------------------------------------
    
    

    //--------------------------------------------------------------------
    
    

    //--------------------------------------------------------------------

    //--------------------------------------------------------------------

    //--------------------------------------------------------------------

    //--------------------------------------------------------------------



    // 或导出数据表的字段名称及自定义配置信息
    public function get_export_field_name($table, $update = 0) {

        // 获取表配置
        $row = $this->db->table('export')->where('name', $table)->get()->getRowArray();
        !$row && $this->table('export')->replace([
            'name' => $table,
            'value' => ''
        ]);
        $value = $row['value'] ? dr_string2array($row['value']) : [];

        // 获取最新的字段信息
        if ($update) {
            $field = $this->db->query('SHOW FULL COLUMNS FROM `'.$table.'`')->getResultArray();
            if (!$field) {
                return $value;
            }
            foreach ($field as $t) {
                if (!isset($value[$t['Field']])) {
                    $value[$t['Field']] = [
                        'use' => 1,
                        'name' => $t['Comment'] ? $t['Comment'] : $t['Field'],
                        'func' => '',
                    ];
                }
            }
            $this->table('export')->replace([
                'name' => $table,
                'value' => dr_array2string($value)
            ]);
        }

        return $value;
    }

    /// 存储导出数据表的字段名称及自定义配置信息
    public function save_export_field_name($table, $data) {

        $this->table('export')->replace([
            'name' => $table,
            'value' => dr_array2string($data)
        ]);

    }
    
}
<?php
namespace Phpcmf\Extend;

use CodeIgniter\HTTP\RequestInterface;

class Security extends \CodeIgniter\Security\Security
{

    public function CSRFVerify(RequestInterface $request)
    {

        if (defined('IS_API') && IS_API) {
            return $this;
        } elseif (APP_DIR == 'weixin') {
            return $this;
        }

        return parent::CSRFVerify($request);
    }

}
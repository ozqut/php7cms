<?php namespace Phpcmf\Home;

// 内容网站表单操作类 基于 Ftable
class Form extends \Phpcmf\Table
{
    public $cid; // 内容id
    public $form; // 表单信息

    public function __construct(...$params) {
        parent::__construct(...$params);
        // 判断表单是否操作
        $cache = \Phpcmf\Service::L('cache')->get('form-'.SITE_ID);
        $this->form = $cache[\Phpcmf\Service::L('Router')->class];
        if (!$this->form) {
            $this->_msg(0, dr_lang('网站表单【%s】不存在',\Phpcmf\Service::L('Router')->class));
            exit;
        }
        // 支持附表存储
        $this->is_data = 1;
        // 模板前缀(避免混淆)
        $this->tpl_name = $this->form['table'];
        $this->tpl_prefix = 'form_';
        // 初始化数据表
        $this->_init([
            'table' => SITE_ID.'_form_'.\Phpcmf\Service::L('Router')->class,
            'field' => $this->form['field'],
            'show_field' => 'title',
        ]);
        // 写入模板
        \Phpcmf\Service::V()->assign([
            'form_name' => $this->form['name'],
            'form_table' => $this->form['table'],
        ]);
    }

    // ========================

    // 内容列表
    protected function _Home_List() {



        // seo
        \Phpcmf\Service::V()->assign(\Phpcmf\Service::L('Seo')->form_list(
            $this->form,
            max(1, (int)\Phpcmf\Service::L('Input')->get('page'))
        ));

        \Phpcmf\Service::V()->assign([
            'urlrule' =>\Phpcmf\Service::L('Router')->form_list_url($this->form['table'], '[page]'),
        ]);
        \Phpcmf\Service::V()->display($this->_tpl_filename('list'));
    }

    // 添加内容
    protected function _Home_Post() {



        // 是否有验证码
        $this->is_post_code = dr_member_auth(
            $this->member_authid,
            $this->member_cache['auth_site'][SITE_ID]['form'][$this->form['table']]['code']
        );

        list($tpl) = $this->_Post(0);

        // seo
        \Phpcmf\Service::V()->assign(\Phpcmf\Service::L('Seo')->form_post($this->form));

        \Phpcmf\Service::V()->assign([
            'form' =>  dr_form_hidden(),
            'rt_url' => $this->form['setting']['rt_url'] ? $this->form['setting']['rt_url'] : dr_now_url(),
            'is_post_code' => $this->is_post_code,
        ]);
        \Phpcmf\Service::V()->display($tpl);
    }

    // 显示内容
    protected function _Home_Show() {



        list($tpl, $data) = $this->_Show(intval(\Phpcmf\Service::L('Input')->get('id')));
        !$data && $this->_msg(0, dr_lang('网站表单内容不存在'));

        // seo
        \Phpcmf\Service::V()->assign(\Phpcmf\Service::L('Seo')->form_show(
            $this->form,
            $data
        ));

        \Phpcmf\Service::V()->display($tpl);
    }


    // ===========================

    // 格式化保存数据 保存之前
    protected function _Format_Data($id, $data, $old) {


        // 验证登录权限
        !$this->uid && $this->_json(0, dr_lang('请登录之后再提交'));


        $data[1]['status'] = 0;
        
        // 默认数据
        $data[0]['uid'] = $data[1]['uid'] = (int)$this->member['uid'];
        $data[1]['author'] = $this->member['username'] ? $this->member['username'] : 'guest';
        $data[1]['inputip'] = \Phpcmf\Service::L('Input')->ip_address();
        $data[1]['inputtime'] = SYS_TIME;
        $data[1]['tableid'] = 0;
        $data[1]['displayorder'] = 0;

        return $data;
    }


    /**
     * 回调处理结果
     * $data
     * */
    protected function _Call_Post($data) {

        // 挂钩点
        \Phpcmf\Hooks::trigger('form_post_after', $data);

        $data[1]['status'] && $this->_json(1, dr_lang('操作成功'));
        // 提醒
        \Phpcmf\Service::M('member')->admin_notice(SITE_ID, 'content', $this->member, dr_lang('%s提交内容审核', $this->form['name']), 'form/'.$this->form['table'].'_verify/edit:id/'.$data[1]['id'], SITE_ID);
        $this->_json(1, dr_lang('操作成功，等待管理员审核'), ['url' => $this->form['setting']['rt_url']]);
    }
}

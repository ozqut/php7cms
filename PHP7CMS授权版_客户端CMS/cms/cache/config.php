<?php return array (
  'space' => 
  array (
    'id' => '1',
    'catid' => '2',
    'title' => 'PHP7CMS演示',
    'thumb' => 'http://demo.php7cms.com/uploadfile/201711/984c864eb3b2ed2.png',
    'keywords' => 'php7cms,官网',
    'description' => 'PHP7CMS采用PHP7+Mysql架构的高效简洁的站群内容管理系统，基于PHPCMF框架平台，功能全面，容易二次开发，为你提供一个全新、快速和优秀的网站解决方案。',
    'hits' => '0',
    'uid' => '1',
    'author' => 'admin',
    'status' => '9',
    'url' => '/index.php?s=space&c=show&id=1',
    'link_id' => '0',
    'tableid' => '0',
    'inputip' => '221.237.149.173',
    'inputtime' => '2017-11-02 11:54:32',
    'updatetime' => '2017-11-02 13:10:59',
    'comments' => '0',
    'favorites' => '0',
    'avgsort' => '0.00',
    'support' => '0',
    'oppose' => '0',
    'donation' => '0.00',
    'displayorder' => '0',
    'weburl' => 'http://demo2.php7cms.com',
    'sync' => 'PHP7CMS78C4FA1B365DE3C49FE2',
    'phone' => '0109929999999',
    'address' => '北京市景德镇110号',
    'map_lng' => '116.466360',
    'map_lat' => '39.923346',
    'lianxiren' => '王春杰',
    'qq' => '10000000',
    'liuyan_total' => '0',
    'flink_total' => '6',
    'urlrule' => '1',
    'content' => '<p>PHP7CMS工作室版权所有&nbsp; 国ICP备字123456号</p>',
    'mtitle' => 'PHP7CMS企业官网演示站点',
    'footer' => NULL,
    'flink' => NULL,
    'banner' => 
    array (
      0 => 
      array (
        'file' => 'http://demo.php7cms.com/uploadfile/201711/ecb91398db446f8.jpg',
        'title' => 'banner',
      ),
      1 => 
      array (
        'file' => 'http://demo.php7cms.com/uploadfile/201711/4a61f78927d2d10.jpg',
        'title' => 'banner',
      ),
    ),
    'tongji' => '<script src="https://s13.cnzz.com/z_stat.php?id=1264530892&web_id=1264530892" language="JavaScript"></script>',
    '_inputtime' => '1509594872',
    '_updatetime' => '1509599459',
    'map' => '116.466360,39.923346',
    '_content' => '&lt;p&gt;PHP7CMS工作室版权所有&amp;nbsp; 国ICP备字123456号&lt;/p&gt;',
    '_banner' => '{"file":["17","18"],"title":["banner","banner"]}',
  ),
  'flink' => 
  array (
    0 => 
    array (
      'title' => 'PHP7群站系统',
      'url' => 'http://www.php7cms.com',
      'logo' => 'http://demo.php7cms.com/uploadfile/201711/e5d12789ceed81a.png',
    ),
    1 => 
    array (
      'title' => 'PHP7企业网站系统',
      'url' => 'http://www.php7cms.com',
      'logo' => 'http://demo.php7cms.com/uploadfile/201711/e5d12789ceed81a.png',
    ),
    2 => 
    array (
      'title' => 'PHP7子站系统',
      'url' => 'http://www.php7cms.com',
      'logo' => 'http://demo.php7cms.com/uploadfile/201711/e5d12789ceed81a.png',
    ),
    3 => 
    array (
      'title' => 'PHP7集团分站系统',
      'url' => 'http://www.php7cms.com',
      'logo' => 'http://demo.php7cms.com/uploadfile/201711/e5d12789ceed81a.png',
    ),
    4 => 
    array (
      'title' => 'PHP7机构分站系统',
      'url' => 'http://www.php7cms.com',
      'logo' => 'http://demo.php7cms.com/uploadfile/201711/e5d12789ceed81a.png',
    ),
    5 => 
    array (
      'title' => 'PHP7CMS官网',
      'url' => 'http://www.php7cms.com',
      'logo' => 'http://demo.php7cms.com/uploadfile/201711/e5d12789ceed81a.png',
    ),
  ),
  'category' => 
  array (
    1 => 
    array (
      'id' => '1',
      'tid' => '1',
      'pid' => '0',
      'mid' => 'news',
      'pids' => '0',
      'name' => '企业新闻',
      'dirname' => 'qiyexinwen',
      'pdirname' => '',
      'child' => '1',
      'childids' => '1,2,3',
      'cid' => '1',
      'uid' => '1',
      'username' => 'admin',
      'thumb' => '',
      'show' => '1',
      'content' => '',
      'setting' => 
      array (
        'linkurl' => '',
        'seo' => 
        array (
          'list_title' => '[第{page}页{join}]{catpname}{SITE_NAME}',
          'list_keywords' => '',
          'list_description' => '',
        ),
        'template' => 
        array (
          'pagesize' => '10',
        ),
      ),
      'displayorder' => '0',
      'url' => '/qiyexinwen/',
      'urlrule' => '/qiyexinwen/p[page].html',
      'catids' => 
      array (
        0 => '1',
        1 => '2',
        2 => '3',
      ),
    ),
    2 => 
    array (
      'id' => '2',
      'tid' => '1',
      'pid' => '1',
      'mid' => 'news',
      'pids' => '0,1',
      'name' => '公司动态',
      'dirname' => 'gongsidongtai',
      'pdirname' => 'qiyexinwen/',
      'child' => '0',
      'childids' => '2',
      'cid' => '1',
      'uid' => '1',
      'username' => 'admin',
      'thumb' => '',
      'show' => '1',
      'content' => '',
      'setting' => 
      array (
        'edit' => 1,
        'template' => 
        array (
          'list' => 'list.html',
          'show' => 'show.html',
          'category' => 'category.html',
          'search' => 'search.html',
          'pagesize' => 20,
        ),
        'seo' => 
        array (
          'list_title' => '[第{page}页{join}]{name}{join}{SITE_NAME}',
          'show_title' => '[第{page}页{join}]{title}{join}{catname}{join}{SITE_NAME}',
        ),
      ),
      'displayorder' => '0',
      'url' => '/gongsidongtai/',
      'urlrule' => '/gongsidongtai/p[page].html',
      'catids' => 
      array (
        0 => '2',
      ),
    ),
    3 => 
    array (
      'id' => '3',
      'tid' => '1',
      'pid' => '1',
      'mid' => 'news',
      'pids' => '0,1',
      'name' => '行业新闻',
      'dirname' => 'xingyexinwen',
      'pdirname' => 'qiyexinwen/',
      'child' => '0',
      'childids' => '3',
      'cid' => '1',
      'uid' => '1',
      'username' => 'admin',
      'thumb' => '',
      'show' => '1',
      'content' => '',
      'setting' => 
      array (
        'edit' => 1,
        'template' => 
        array (
          'list' => 'list.html',
          'show' => 'show.html',
          'category' => 'category.html',
          'search' => 'search.html',
          'pagesize' => 20,
        ),
        'seo' => 
        array (
          'list_title' => '[第{page}页{join}]{name}{join}{SITE_NAME}',
          'show_title' => '[第{page}页{join}]{title}{join}{catname}{join}{SITE_NAME}',
        ),
      ),
      'displayorder' => '0',
      'url' => '/xingyexinwen/',
      'urlrule' => '/xingyexinwen/p[page].html',
      'catids' => 
      array (
        0 => '3',
      ),
    ),
    4 => 
    array (
      'id' => '4',
      'tid' => '1',
      'pid' => '0',
      'mid' => 'image',
      'pids' => '0',
      'name' => '公司产品',
      'dirname' => 'gongsichanpin',
      'pdirname' => '',
      'child' => '0',
      'childids' => '4',
      'cid' => '1',
      'uid' => '1',
      'username' => 'admin',
      'thumb' => '',
      'show' => '1',
      'content' => '',
      'setting' => 
      array (
        'edit' => 1,
        'template' => 
        array (
          'list' => 'list.html',
          'show' => 'show.html',
          'category' => 'category.html',
          'search' => 'search.html',
          'pagesize' => 20,
        ),
        'seo' => 
        array (
          'list_title' => '[第{page}页{join}]{name}{join}{SITE_NAME}',
          'show_title' => '[第{page}页{join}]{title}{join}{catname}{join}{SITE_NAME}',
        ),
      ),
      'displayorder' => '0',
      'url' => '/gongsichanpin/',
      'urlrule' => '/gongsichanpin/p[page].html',
      'catids' => 
      array (
        0 => '4',
      ),
    ),
    5 => 
    array (
      'id' => '5',
      'tid' => '1',
      'pid' => '0',
      'mid' => 'image',
      'pids' => '0',
      'name' => '经典案例',
      'dirname' => 'jingdiananli',
      'pdirname' => '',
      'child' => '0',
      'childids' => '5',
      'cid' => '1',
      'uid' => '1',
      'username' => 'admin',
      'thumb' => '',
      'show' => '1',
      'content' => '',
      'setting' => 
      array (
        'edit' => 1,
        'template' => 
        array (
          'list' => 'list.html',
          'show' => 'show.html',
          'category' => 'category.html',
          'search' => 'search.html',
          'pagesize' => 20,
        ),
        'seo' => 
        array (
          'list_title' => '[第{page}页{join}]{name}{join}{SITE_NAME}',
          'show_title' => '[第{page}页{join}]{title}{join}{catname}{join}{SITE_NAME}',
        ),
      ),
      'displayorder' => '0',
      'url' => '/jingdiananli/',
      'urlrule' => '/jingdiananli/p[page].html',
      'catids' => 
      array (
        0 => '5',
      ),
    ),
    6 => 
    array (
      'id' => '6',
      'tid' => '0',
      'pid' => '0',
      'mid' => '',
      'pids' => '0',
      'name' => '招聘信息',
      'dirname' => 'zhaopinxinxi',
      'pdirname' => '',
      'child' => '0',
      'childids' => '6',
      'cid' => '1',
      'uid' => '1',
      'username' => 'admin',
      'thumb' => '',
      'show' => '1',
      'content' => '&lt;h3 class=&quot;job-tit&quot; style=&quot;margin: 0px; padding: 0px; font-size: 18px; font-weight: 500; font-family: &amp;quot;Open Sans&amp;quot;, Arial, &amp;quot;Hiragino Sans GB&amp;quot;, &amp;quot;Microsoft YaHei&amp;quot;, 微软雅黑, STHeiti, &amp;quot;WenQuanYi Micro Hei&amp;quot;, SimSun, sans-serif, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);&quot;&gt;&lt;strong&gt;商务拓展/大客户/BD销售经理&lt;/strong&gt;&lt;/h3&gt;&lt;p&gt;岗位职责：&lt;br/&gt;1、根据公司业务需求拓展新客户，维护合作关系，制定个性化的营销方案，完成公司制定的销售任务&lt;br/&gt;2、负责创业邦全媒体（网站、杂志、新媒体）资源售卖，广告客户开发；&lt;br/&gt;3、创业邦品牌活动资源售卖及客户开发；&lt;br/&gt;4、负责带领团队进行沟通、谈判、合同签订等流程管理，对合作项目的实施过程和结果负责；&lt;br/&gt;5、带领团队完成公司制定的工作目标及销售业绩；&lt;br/&gt;6、发起、协调及配合内部相关同事按客户要求完成项目实施执行；&lt;br/&gt;任职要求：&lt;br/&gt;1、学历：大专及以上学历；专业不限，年龄30岁左右。&lt;br/&gt;2、工作经历：5年以上媒体广告销售工作经验 ，有政府、投融资机构客户资源，汽车行业客户及国际品&lt;br/&gt;牌客户经历和资源的优先；&lt;br/&gt;3、性格特质：德行端正，性格开朗、具有感染力和激情，做事认真并具有良好的执行力&lt;br/&gt;4、技能：具备良好的沟通技巧、协调能力以及商务谈判技巧，可撰写方案并且熟练操作office办公软件&lt;br/&gt;5、期待：热爱生活，愿意投身创投领域并渴望与我们一起成长的小伙伴加入&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;h3 class=&quot;job-tit&quot; style=&quot;margin: 0px; padding: 0px; font-size: 18px; font-weight: 500; font-family: &amp;quot;Open Sans&amp;quot;, Arial, &amp;quot;Hiragino Sans GB&amp;quot;, &amp;quot;Microsoft YaHei&amp;quot;, 微软雅黑, STHeiti, &amp;quot;WenQuanYi Micro Hei&amp;quot;, SimSun, sans-serif, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);&quot;&gt;&lt;strong&gt;广告销售经理&lt;/strong&gt;&lt;/h3&gt;&lt;p&gt;岗位职责：&lt;br/&gt;1、负责自有品牌活动赞助、定制活动合作、线上线下广告销售（杂志、网站、微博微信等产品），整合公司资源，完成公司制定的销售目标；&lt;br/&gt;2、制定销售计划，并每周按计划拜访客户和开发新客户，完成销售的任务季度、年度业绩指标；&lt;br/&gt;3、寻找潜在客户进行有效沟通，了解客户需求，为客户提供专业化服务，完成销售业绩；&lt;br/&gt;4、维护客户关系，沟通解决客户问题，定期汇报销售工作及总结；&lt;br/&gt;5、完成公司交办的其它临时性工作。&lt;br/&gt;&lt;br/&gt;任职要求：&lt;br/&gt;1、本科以上学历，市场、商务相关专业&lt;br/&gt;2、热爱销售工作，勤奋努力、有敏锐的市场触觉，较强的独立开拓和挖掘客户能力；&lt;br/&gt;3、具备大型展览活动赞助、广告销售经验，对媒体及广告行业有深刻理解，有客户资源者优先；&amp;nbsp;&lt;br/&gt;4、具备品牌客户资源、五百强企业、政府园区资源、投资机构、金融机构资源者优先考虑；&lt;br/&gt;5、有强烈的进取精神，思维敏捷、具有优秀的沟通以及谈判能力；&lt;br/&gt;6、重视团队合作，责任感强，有良好的心理素质，乐观积极，敢于承受压力，接受挑战。&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;',
      'setting' => 
      array (
        'linkurl' => '',
        'seo' => 
        array (
          'list_title' => '[第{page}页{join}]{name}{join}{SITE_NAME}',
          'list_keywords' => '',
          'list_description' => '',
        ),
        'template' => 
        array (
          'pagesize' => '20',
        ),
      ),
      'displayorder' => '0',
      'url' => '/zhaopinxinxi/',
      'urlrule' => '/zhaopinxinxi/p[page].html',
      'catids' => 
      array (
        0 => '6',
      ),
    ),
    7 => 
    array (
      'id' => '7',
      'tid' => '0',
      'pid' => '0',
      'mid' => '',
      'pids' => '0',
      'name' => '关于我们',
      'dirname' => 'guanyuwomen',
      'pdirname' => '',
      'child' => '1',
      'childids' => '7,8,9',
      'cid' => '1',
      'uid' => '1',
      'username' => 'admin',
      'thumb' => '',
      'show' => '1',
      'content' => '',
      'setting' => 
      array (
        'linkurl' => '',
        'getchild' => '1',
        'seo' => 
        array (
          'list_title' => '[第{page}页{join}]{name}{join}{SITE_NAME}',
          'list_keywords' => '',
          'list_description' => '',
        ),
        'template' => 
        array (
          'pagesize' => '20',
        ),
      ),
      'displayorder' => '0',
      'url' => '/guanyuwomen/',
      'urlrule' => '/guanyuwomen/p[page].html',
      'catids' => 
      array (
        0 => '7',
        1 => '8',
        2 => '9',
      ),
    ),
    8 => 
    array (
      'id' => '8',
      'tid' => '0',
      'pid' => '7',
      'mid' => '',
      'pids' => '0,7',
      'name' => '公司介绍',
      'dirname' => 'gongsijieshao',
      'pdirname' => 'guanyuwomen/',
      'child' => '0',
      'childids' => '8',
      'cid' => '1',
      'uid' => '1',
      'username' => 'admin',
      'thumb' => '',
      'show' => '1',
      'content' => '&lt;p&gt;PHP7CMS采用PHP7+Mysql架构的高效简洁的站群内容管理系统，基于PHPCMF框架平台，功能全面，容易二次开发，为你提供一个全新、快速和优秀的网站解决方案。PHP7CMS采用PHP7+Mysql架构的高效简洁的站群内容管理系统，基于PHPCMF框架平台，功能全面，容易二次开发，为你提供一个全新、快速和优秀的网站解决方案。PHP7CMS采用PHP7+Mysql架构的高效简洁的站群内容管理系统，基于PHPCMF框架平台，功能全面，容易二次开发，为你提供一个全新、快速和优秀的网站解决方案。PHP7CMS采用PHP7+Mysql架构的高效简洁的站群内容管理系统，基于PHPCMF框架平台，功能全面，容易二次开发，为你提供一个全新、快速和优秀的网站解决方案。&lt;/p&gt;&lt;p&gt;PHP7CMS采用PHP7+Mysql架构的高效简洁的站群内容管理系统，基于PHPCMF框架平台，功能全面，容易二次开发，为你提供一个全新、快速和优秀的网站解决方案。&lt;/p&gt;&lt;p&gt;PHP7CMS采用PHP7+Mysql架构的高效简洁的站群内容管理系统，基于PHPCMF框架平台，功能全面，容易二次开发，为你提供一个全新、快速和优秀的网站解决方案。&lt;/p&gt;&lt;p&gt;PHP7CMS采用PHP7+Mysql架构的高效简洁的站群内容管理系统，基于PHPCMF框架平台，功能全面，容易二次开发，为你提供一个全新、快速和优秀的网站解决方案。&lt;/p&gt;',
      'setting' => 
      array (
        'linkurl' => '',
        'seo' => 
        array (
          'list_title' => '[第{page}页{join}]{name}{join}{SITE_NAME}',
          'list_keywords' => '',
          'list_description' => '',
        ),
        'template' => 
        array (
          'pagesize' => '20',
        ),
      ),
      'displayorder' => '0',
      'url' => '/gongsijieshao/',
      'urlrule' => '/gongsijieshao/p[page].html',
      'catids' => 
      array (
        0 => '8',
      ),
    ),
    9 => 
    array (
      'id' => '9',
      'tid' => '0',
      'pid' => '7',
      'mid' => '',
      'pids' => '0,7',
      'name' => '联系我们',
      'dirname' => 'lianxiwomen',
      'pdirname' => 'guanyuwomen/',
      'child' => '0',
      'childids' => '9',
      'cid' => '1',
      'uid' => '1',
      'username' => 'admin',
      'thumb' => '',
      'show' => '1',
      'content' => '&lt;p style=&quot;box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; padding: 0px; line-height: 30px; font-size: 16px; color: rgb(51, 51, 51); font-family: 微软雅黑; white-space: normal; background-color: rgb(249, 249, 249);&quot;&gt;电话：0100000000&lt;/p&gt;&lt;p style=&quot;box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; padding: 0px; line-height: 30px; font-size: 16px; color: rgb(51, 51, 51); font-family: 微软雅黑; white-space: normal; background-color: rgb(249, 249, 249);&quot;&gt;QQ号：1917785500&lt;/p&gt;&lt;p style=&quot;box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; padding: 0px; line-height: 30px; font-size: 16px; color: rgb(51, 51, 51); font-family: 微软雅黑; white-space: normal; background-color: rgb(249, 249, 249);&quot;&gt;联系人：王春杰&lt;/p&gt;&lt;p&gt;&lt;img width=&quot;530&quot; height=&quot;340&quot; src=&quot;http://api.map.baidu.com/staticimage?center=116.513234,39.868941&amp;zoom=12&amp;width=530&amp;height=340&amp;markers=116.583374,39.822851&quot;/&gt;&lt;/p&gt;',
      'setting' => 
      array (
        'linkurl' => '',
        'seo' => 
        array (
          'list_title' => '[第{page}页{join}]{name}{join}{SITE_NAME}',
          'list_keywords' => '',
          'list_description' => '',
        ),
        'template' => 
        array (
          'pagesize' => '20',
        ),
      ),
      'displayorder' => '0',
      'url' => '/lianxiwomen/',
      'urlrule' => '/lianxiwomen/p[page].html',
      'catids' => 
      array (
        0 => '9',
      ),
    ),
  ),
  'category_dir' => 
  array (
    'qiyexinwen' => '1',
    'gongsidongtai' => '2',
    'xingyexinwen' => '3',
    'gongsichanpin' => '4',
    'jingdiananli' => '5',
    'zhaopinxinxi' => '6',
    'guanyuwomen' => '7',
    'gongsijieshao' => '8',
    'lianxiwomen' => '9',
  ),
);